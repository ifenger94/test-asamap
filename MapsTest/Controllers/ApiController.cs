﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Asa.DataStore;
using Asa.MapApi.Models.ViewModels;

namespace Asa.MapApi.Controllers
{
    public class ApiController : Controller
    {
        // GET: Api
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Categories(string id, Dictionary<string, object> entity)
        {
            // sheetName: Categorias
            try
            {
                switch (Request.HttpMethod)
                {
                    case "GET":
                        var listCategories = _ListCategory().ToArray();
                        return Json(listCategories, JsonRequestBehavior.AllowGet);
                }

                Response.StatusCode = 400;
                return Json(new { error = "Method not suported." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Response.StatusCode = 500;
                return Json(new { error = e.Message }, JsonRequestBehavior.AllowGet);
            }

        }


        public ActionResult ValidateForm(FormViewModel entity, string coordinates)
        {
            // Se agrega control de errores.
            try
            {
                if (Request.HttpMethod == "POST")
                {
                    var x_y = coordinates.Split(',');
                    entity.X = Convert.ToDouble(x_y[0].Replace('.', ','));
                    entity.Y = Convert.ToDouble(x_y[1].Replace('.', ','));

                    bool isvalid = true;
                    string message = "Ok";

                    // Validaciones

                    if (entity.X < -180 | entity.X > 180 | entity.Y < -90 | entity.Y > 90)
                    {
                        isvalid = false;
                        message = string.Concat("The coordinates are invalid.");
                    }

                    return Json(new { entity = entity, isvalid, errors = message, JsonRequestBehavior.AllowGet });
                }

                Response.StatusCode = 400;
                return Json(new { error = "Method not suported." }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception e)
            {
                Response.StatusCode = 500;
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult POIs(string id, Dictionary<string, object> entity)
        {
            //Agrego bloque try/catch para controlar la posibiladad de algun error.

            try
            {
                switch (Request.HttpMethod)
                {
                    case "GET":
                        var _POIs = this._ListPOIS();
                        return Json(new { pois = _POIs.ToArray() }, JsonRequestBehavior.AllowGet);
                    case "POST":
                        //InsertPOI (entity)
                        break;
                    case "PUT":
                        //UpdatePOI (id, entity)
                        break;
                    case "DELETE":
                        //DeletePOI (id)
                        break;
                }

                Response.StatusCode = 400;
                return Json(new { error = "Method not suported." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Response.StatusCode = 500;
                return Json(e.Message, JsonRequestBehavior.AllowGet);
            }
        }

        private List<IDictionary<string, object>> _ListPOIS()
        {
            var driver = new XlsDriver();

            // La ruta base no deberia estar hardcodeada.
            //var conn = driver.Connect(@"C:\Users\Entrevista\Documents\Visual Studio 2015\Projects\MapsTest\MapsTest\bin\Data\ds.xls");

            var path = string.Concat(AppDomain.CurrentDomain.BaseDirectory, @"Data\ds.xls");
            var conn = driver.Connect(path);

            try
            {
                conn.Open();
                var dt = driver.ListData(conn, "POIs");
                var data = new List<IDictionary<string, object>>();
                foreach (DataRow row in dt.Rows)
                {
                    IDictionary<string, object> props = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        props.Add(col.ColumnName.Replace(" ", "").Replace("(", "").Replace(")", ""), row[col.Ordinal]);
                    }

                    data.Add(props);

                    // El return debe estar fuera del foreach, si no solo cargara el primer registro encontrado.
                    //return data;
                }

                return data;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                conn.Close();
            }

        }

        /// <summary>
        /// Obtiene una lista de categorias ordenada de a-z por el campo value.
        /// </summary>
        /// <returns></returns>
        private List<CategoryViewModel> _ListCategory()
        {
            var driver = new XlsDriver();
            var data = new List<CategoryViewModel>();
            var path = string.Concat(AppDomain.CurrentDomain.BaseDirectory, @"Data\ds.xls");
            var conn = driver.Connect(path);

            try
            {
                conn.Open();
                var dt = driver.ListData(conn, "Categories");
                foreach (DataRow row in dt.Rows)
                {
                    var category = new CategoryViewModel()
                    {
                        id = row.Field<string>("Id"),
                        value = row.Field<string>("Value")
                    };

                    data.Add(category);
                }

                return data.OrderBy(e => e.value).ToList();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                conn.Close();
            }

        }

    }
}
