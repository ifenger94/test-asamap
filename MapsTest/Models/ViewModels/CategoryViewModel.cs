﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Asa.MapApi.Models.ViewModels
{
    public class CategoryViewModel
    {
        public string id { set; get; }
        public string value { set; get; }
    }
}