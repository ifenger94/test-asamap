﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Asa.MapApi.Models.ViewModels
{
    public class FormViewModel
    {
        // Genero el modelo correspondiente para el formulario

        [MinLength(20)]
        public string Nombre { set; get; }
        public string Direccion { set; get; }
        public string Telefono { set; get; }
        public string Categoria { set; get; }
        public double X { set; get; }
        public double Y { set; get; }
    }
}