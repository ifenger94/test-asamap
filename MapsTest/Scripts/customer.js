﻿
    $(document).ready(function () {
        $("#addPoi").click(function () {

            var options = {};
            options.url = "/api/Categories";
            options.type = "GET";
            options.dataType = "json";
            options.success = function (data) {
                $("#selectCustom").empty().append('<option value="null" disabled selected>Seleccionar</option>');

                data.forEach(function (element) {
                    $("#selectCustom").append('<option class="remove-option" value="' + element.id + '">' + element.value + '</option>');
                });

            };
            options.error = function () {
                console.log("Error while calling the Web API!");
            };

            $.ajax(options);
        });
    });
