# homework-net-2K20
Prueba técnica desarrolladores .net

Observaciones: 

- Los pois no se renderizan bien en el mapa. Probar en modo incognito en google o firefox funciona perfectamente. 
- Se adjunta una imagen con los pois renderizados utilizando firefox.


Modificaciones Realizadas:
1) Se agregó control de errores.
2) Se crearon los modelos correspondientes al formulario de agregación de Pois y el modelo para lista de categorías.
3) Se agregaron paths dinámicos para conexión con los archivos .xls, para que no allá problemas al ejecutar la solución en otro equipo.
4) Se creó un script de js con el nombre de customer en la carpeta script, este se encarga de llamar al endpoint que llenara la lista 
	de categorías en el formulario de agregación de Pois.
5) Se modificó de color rojo a negro el icono de los puntos.
6) Se agregó las validaciones descriptas en el objetivo 2.
7) Se agregó el método _ListCategory() para cargar desde el archivo .xls la lista correspondiente.
